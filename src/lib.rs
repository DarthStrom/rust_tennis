pub fn game<'a>(player1: &'a str, player2: &'a str) -> Game<'a> {
    Game { player1: Player { name: player1, points: 0 },
           player2: Player { name: player2, points: 0 },
           over: false }
}

struct Player<'a> {
    name: &'a str,
    points: i32,
}

pub struct Game<'a> {
    player1: Player<'a>,
    player2: Player<'a>,
    over: bool,
}

impl<'a> Game<'a> {
    pub fn point(&mut self, player: &str) {
        if !self.over {
            if self.player1.name == player {
                self.player1.points += 1;
            } else if self.player2.name == player {
                self.player2.points += 1;
            }
        }
    }

    pub fn score(&mut self) -> String {
        match (self.player1.points, self.player2.points) {
            (a, b) if winning(a, b) => {
                self.over = true;
                ["game", self.player1.name].join(" ")
            },
            (a, b) if winning(b, a) => {
                self.over = true;
                ["game", self.player2.name].join(" ")
            },
            (a, b) if a == b => tied_at(self.player1.points),
            (a, b) if a + b > 6 => stage2_score(&self.player1, &self.player2),
            _ => stage1_score(&self.player1, &self.player2)
        }
    }
}

fn winning(w: i32, l: i32) -> bool {
    (w - l > 1 || l < 3) && w > 3
}

fn score_for<'a>(points: i32) -> &'a str {
    match points {
        1 => "fifteen",
        2 => "thirty",
        3 => "forty",
        _ => "love"
    }
}

fn tied_at(points: i32) -> String {
    match points {
        0 ... 2 => [score_for(points), "all"].join(" "),
        _ => String::from("deuce")
    }
}

fn stage2_score(player1: &Player, player2: &Player) -> String {
    if player1.points > player2.points {
        ["advantage", player1.name].join(" ")
    } else {
        ["advantage", player2.name].join(" ")
    }
}

fn stage1_score(player1: &Player, player2: &Player) -> String {
    let player1_score = [player1.name, score_for(player1.points)].join(" ");
    let player2_score = [player2.name, score_for(player2.points)].join(" ");
    [player1_score, player2_score].join(", ")
}