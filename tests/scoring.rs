extern crate tennis;

#[test]
fn alice_shut_out() {
    let mut game = check_game_start();
    check_score(&mut game, "Alice", "Alice fifteen, Bob love");
    check_score(&mut game, "Alice", "Alice thirty, Bob love");
    check_score(&mut game, "Alice", "Alice forty, Bob love");
    check_score(&mut game, "Alice", "game Alice");
}

#[test]
fn bob_shut_out() {
    let mut game = check_game_start();
    check_score(&mut game, "Bob", "Alice love, Bob fifteen");
    check_score(&mut game, "Bob", "Alice love, Bob thirty");
    check_score(&mut game, "Bob", "Alice love, Bob forty");
    check_score(&mut game, "Bob", "game Bob");
}

#[test]
fn alternating() {
    let mut game = check_game_start();
    check_score(&mut game, "Alice", "Alice fifteen, Bob love");
    check_score(&mut game, "Bob", "fifteen all");
    check_score(&mut game, "Alice", "Alice thirty, Bob fifteen");
    check_score(&mut game, "Bob", "thirty all");
    check_score(&mut game, "Alice", "Alice forty, Bob thirty");
    check_score(&mut game, "Bob", "deuce");
    check_score(&mut game, "Alice", "advantage Alice");
    check_score(&mut game, "Bob", "deuce");
    check_score(&mut game, "Bob", "advantage Bob");
    check_score(&mut game, "Bob", "game Bob");
    check_score(&mut game, "Alice", "game Bob");
}

#[test]
fn bad_player_name() {
    let mut game = check_game_start();
    check_score(&mut game, "Nobody", "love all");
}

fn check_game_start<'a>() -> tennis::Game<'a> {
    let mut game = tennis::game("Alice", "Bob");
    assert_eq!("love all", game.score());
    game
}

fn check_score(game: &mut tennis::Game, player: &str, expected_score: &str) {
    game.point(player);
    assert_eq!(expected_score, game.score());
}